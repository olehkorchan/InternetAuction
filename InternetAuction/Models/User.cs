﻿using Microsoft.AspNetCore.Identity;

namespace InternetAuction.Models
{
    /// <summary>
    /// Model of user
    /// </summary>
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
    }
}
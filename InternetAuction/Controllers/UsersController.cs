﻿using System.Linq;
using System.Threading.Tasks;
using InternetAuction.Models;
using InternetAuction.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InternetAuction.Controllers
{
    /// <summary>
    /// Users controller.
    /// </summary>
    /// <remarks>
    /// Display Index page with all users.
    /// </remarks>
    /// <remarks>
    /// Implements CRUD functionality for user model.
    /// </remarks>
    [Authorize(Roles = "master, admin")]
    public class UsersController : Controller
    {
        private readonly UserManager<User> _userManager;
 
        /// <summary>
        /// Initializes a new instance of the <see cref="UsersController"/> class.
        /// </summary>
        /// <param name="userManager"></param>
        public UsersController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }
        
        /// <summary>
        /// Display page with all users.
        /// </summary>
        /// <returns>View with all users.</returns>
        public IActionResult Index() => View(_userManager.Users.ToList());
 
        /// <summary>
        /// Display page witch data form to create new user.
        /// </summary>
        /// <returns>View with data form to create new user.</returns>
        public IActionResult Create() => View();
 
        /// <summary>
        /// Get data from form and create new user.
        /// </summary>
        /// <param name="model">New user data.</param>
        /// <returns>View with all users <see cref="Index"/>.</returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { Email = model.Login, UserName = model.Login };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }
 
        /// <summary>
        /// Display page witch data form of current user.
        /// </summary>
        /// <param name="id">Identifier of current user.</param>
        /// <returns>View with data form of current user.</returns>
        public async Task<IActionResult> Edit(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return RedirectToAction("Error", "Home");
            }
            EditUserViewModel model = new EditUserViewModel {Id = user.Id, UserName = user.Email, Name = user.Name, LastName = user.LastName, City = user.City, PhoneNumber = user.PhoneNumber};
            return View(model);
        }
 
        /// <summary>
        /// Get data from form and update current user data.
        /// </summary>
        /// <param name="model">New data for current user.</param>
        /// <returns>View with all users <see cref="Index"/>.</returns>
        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);
                if(user!=null)
                {
                    user.Email = model.UserName;
                    user.UserName = model.UserName;
                    user.Name = model.Name;
                    user.LastName = model.LastName;
                    user.City = model.City;
                    user.PhoneNumber = model.PhoneNumber;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            return View(model);
        }
 
        /// <summary>
        /// Delete current user.
        /// </summary>
        /// <param name="id">Identifier of current user</param>
        /// <returns>View with all users <see cref="Index"/>.</returns>
        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }
    }
}
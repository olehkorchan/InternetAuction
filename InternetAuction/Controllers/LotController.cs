﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using InternetAuction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace InternetAuction.Controllers
{
    /// <summary>
    /// Controller that contains implementation crud functionality to lot model.
    /// </summary>
    /// <remarks>
    /// Can create, update, delete lots.
    /// </remarks>
    /// <remarks>
    /// Display lot page, user lots pages and implement "pay" functionality.
    /// </remarks>
    [Authorize]
    public class LotController : Controller
    {
        private readonly ApplicationContext _db;
        private readonly UserManager<User> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        /// <summary>
        /// Initializes a new instance of the <see cref="LotController"/> class.
        /// </summary>
        /// <param name="db">database context</param>
        /// <param name="userManager"></param>
        /// <param name="webHostEnvironment"></param>
        public LotController(ApplicationContext db, UserManager<User> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _webHostEnvironment = webHostEnvironment;
            _userManager = userManager;
        }

        /// <summary>
        /// Display page with current lot data.
        /// </summary>
        /// <param name="id">Id of current lot</param>
        /// <returns>View with current lot data.</returns>
        public async Task<IActionResult> LotPage(int id)
        {
            var model = await _db.Lots.FirstOrDefaultAsync(i => i.Id == id);
            return View(model);
        }

        /// <summary>
        /// Display all lots of current user
        /// </summary>
        /// <returns>View with all lots of current user.</returns>
        public async Task<IActionResult> UserLots()
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var model = _db.Lots.Where(i => i.UserId == currentUser.Id).ToList();
            return View(model);
        }

        /// <summary>
        /// Display all current user bet lots that ready to buy.
        /// </summary>
        /// <returns>View with all current user bet lots that ready to buy.</returns>
        public async Task<IActionResult> LotsToBuy()
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var model = _db.Lots.Where(i => i.BuyerId == currentUser.Id).ToList();
            return View(model);
        }
        
        /// <summary>
        /// Method which implement "pay" functionality.
        /// </summary>
        /// <param name="id">Identifier of current lot</param>
        /// <returns>View <see cref="LotsToBuy"/>.</returns>
        [HttpPost]
        public async Task<IActionResult> Pay(int id)
        {
            if (id != 0)
            {
                var lot = await _db.Lots.FirstOrDefaultAsync(i => i.Id == id);
                if (lot != null)
                {
                    _db.Remove(lot);
                    await _db.SaveChangesAsync();
                    return RedirectToAction("LotsToBuy");
                }
            }

            return RedirectToAction("Error", "Home");
        }
        /// <summary>
        /// Image resize and save in wwwroot/images folder
        /// </summary>
        /// <param name="image">Uploaded file</param>
        /// <param name="maxWidth">Max width to compress</param>
        /// <param name="filePath">Determined path to save file</param>
        private static async Task ResizeAndUploadImageAsync(Image image, int maxWidth, string 
            filePath)
        {
            await using var writeStream = new MemoryStream();
            if (image.Width > maxWidth)
            {
                var thumbScaleFactor = maxWidth / image.Width;
                image.Mutate(i => i.Resize(maxWidth, image.Height * 
                                                     thumbScaleFactor));
            }

            await image.SaveAsPngAsync(writeStream);
            IFormFile file = new FormFile(writeStream, 0, writeStream.Length, "name", "fileName");
            await file.CopyToAsync(new FileStream(filePath, FileMode.Create));
        }
        
        /// <summary>
        /// Display page with data form of new lot.
        /// </summary>
        /// <returns>View with data form of new lot.</returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Get data from form and create a new Lot in database.
        /// </summary>
        /// <param name="lot">Lot data</param>
        /// <param name="image">Uploaded file</param>
        /// <returns>Home page.</returns>
        [HttpPost]
        public async Task<IActionResult> Add(Lot lot, IFormFile image)
        {
            if (image != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                string uniqueFileName = Guid.NewGuid() + "_" + image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                await using (var stream = image.OpenReadStream())
                using (var toCompress = await Image.LoadAsync(stream))
                {
                    await ResizeAndUploadImageAsync(toCompress, 300, filePath);
                }
                lot.Img = uniqueFileName;
            }
            lot.CurrentValue = lot.StartValue;
            _db.Lots.Add(lot);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Display page with data of current lot
        /// </summary>
        /// <param name="id">Current lot id</param>
        /// <returns>View with form of current lot data.</returns>
        public async Task<IActionResult> Edit(int id)
        {
            
            if (id != 0)
            {
                var currentDate = Convert.ToDateTime(DateTime.Now.ToString(CultureInfo.CurrentCulture));
                var lot = await _db.Lots.FirstOrDefaultAsync(i => i.Id == id);
                var currentUser = await _userManager.GetUserAsync(User);
                var isInRole = await _userManager.IsInRoleAsync(currentUser, "master") || await _userManager.IsInRoleAsync(currentUser, "admin") || await _userManager.IsInRoleAsync(currentUser, "moderator");
                if (lot != null && (lot.UserId == currentUser.Id || isInRole) && lot.FinishDate > currentDate) return View(lot);
            }

            return RedirectToAction("Error", "Home");
        }

        /// <summary>
        /// Get new data from form and update current lot.
        /// </summary>
        /// <param name="lot">Lot data</param>
        /// <param name="image">Uploaded file</param>
        /// <returns>Home page.</returns>
        [HttpPost]
        public async Task<IActionResult> Edit(Lot lot, IFormFile image)
        {
            if (image != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                string uniqueFileName = Guid.NewGuid().ToString() + "_" + image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                await using (var stream = image.OpenReadStream())
                using (var toCompress = await Image.LoadAsync(stream))
                {
                    await ResizeAndUploadImageAsync(toCompress, 300, filePath);
                }
                lot.Img = uniqueFileName;
            }
            _db.Lots.Update(lot);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
        /// <summary>
        /// Delete current lot from database.
        /// </summary>
        /// <param name="id">Identifier of current lot</param>
        /// <returns>Home page.</returns>
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            if (id != 0)
            {
                Lot lot = await _db.Lots.FirstOrDefaultAsync(i => i.Id == id);
                if (lot != null)
                {
                    var path = Path.Combine(Directory.GetCurrentDirectory(),"wwwroot\\images\\" + lot.Img);
                    if(System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    _db.Remove(lot);
                    await _db.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }
            }

            return RedirectToAction("Error", "Home");
        }
        
        /// <summary>
        /// Can save new price for lot from user bet.
        /// </summary>
        /// <param name="id">Current lot identifier</param>
        /// <param name="newBet">New price</param>
        /// <param name="buyerId"></param>
        /// <returns>Lot page <see cref="LotPage"/>.</returns>
        public async Task<IActionResult> UpgradeValue(int id, int newBet, string buyerId)
        {
            if (id == 0 || buyerId == null) return NotFound();
            var lot = await _db.Lots.FirstOrDefaultAsync(i => i.Id  == id);
            if (lot == null) return NotFound();
            lot.CurrentValue = newBet;
            lot.BuyerId = buyerId;
            _db.Update(lot);
            await _db.SaveChangesAsync();
            return RedirectToAction("LotPage", new { id });

        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetAuction.Models;
using InternetAuction.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InternetAuction.Controllers
{
    /// <summary>
    /// User roles controller.
    /// </summary>
    /// <remarks>
    /// Display Index page with all roles.
    /// </remarks>
    /// <remarks>
    /// Implement CRUD functionality for user roles.
    /// </remarks>
    [Authorize(Roles = "master")]
    public class RolesController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<User> _userManager;
        /// <summary>
        /// Initializes a new instance of the <see cref="RolesController"/> class.
        /// </summary>
        /// <param name="roleManager"></param>
        /// <param name="userManager"></param>
        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<User> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
        /// <summary>
        /// Display page with all roles
        /// </summary>
        /// <returns>View with all roles.</returns>
        public IActionResult Index() => View(_roleManager.Roles.ToList());
 
        /// <summary>
        /// Display page with data form to create new role.
        /// </summary>
        /// <returns>View with data form.</returns>
        public IActionResult Create() => View();
        /// <summary>
        /// Get data from form and create new role.
        /// </summary>
        /// <param name="name">name of new role</param>
        /// <returns>Index view <see cref="Index"/>.</returns>
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }
         
        /// <summary>
        /// Delete current role
        /// </summary>
        /// <param name="id">Identifier of current role</param>
        /// <returns>Index view <see cref="Index"/>.</returns>
        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }
 
        /// <summary>
        /// Display list of users
        /// </summary>
        /// <returns>View with all users</returns>
        public IActionResult UserList() => View(_userManager.Users.ToList());
 
        /// <summary>
        /// Display page with form of choosing role for current user.
        /// </summary>
        /// <param name="userId">Current user identifier</param>
        /// <returns>View with choosing form.</returns>
        public async Task<IActionResult> Edit(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if(user!=null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }
 
            return RedirectToAction("Error", "Home");
        }
        /// <summary>
        /// Get data from form and change roles of user.
        /// </summary>
        /// <param name="userId">Identifier of user to be changed</param>
        /// <param name="roles">List with roles which was chosen</param>
        /// <returns>View with all users <see cref="UserList"/>.</returns>
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if(user!=null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                var addedRoles = roles.Except(userRoles);
                var removedRoles = userRoles.Except(roles);
 
                await _userManager.AddToRolesAsync(user, addedRoles);
 
                await _userManager.RemoveFromRolesAsync(user, removedRoles);
 
                return RedirectToAction("UserList");
            }
 
            return RedirectToAction("Error", "Home");
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using InternetAuction.Models;
using InternetAuction.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InternetAuction.Controllers
{
    /// <summary>
    /// Main page controller.
    /// </summary>
    /// <remarks>
    /// Display Index page and Error page.
    /// </remarks>
    /// <remarks>
    /// Allow user to edit his profile.
    /// </remarks>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationContext _db;
        private readonly UserManager<User> _userManager;
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="db">database context</param>
        /// <param name="userManager"></param>
        public HomeController(ILogger<HomeController> logger, ApplicationContext db, UserManager<User> userManager)
        {
            _db = db;
            _logger = logger;
            _userManager = userManager;

        }
        
        /// <summary>
        /// Display main page.
        /// </summary>
        /// <param name="searchString">String with searching query</param>
        /// <returns>View with all searched lots.</returns>
        public async Task<IActionResult> Index(string searchString)
        {
            ViewData["CurrentFilter"] = searchString;
            var timeNow = Convert.ToDateTime(DateTime.Now.ToString(CultureInfo.CurrentCulture));
            var lots = _db.Lots.Where(i => i.FinishDate > timeNow );
            if (!string.IsNullOrEmpty(searchString))
            {
                lots = lots.Where(l => l.Name.Contains(searchString) || l.City.Contains(searchString));
            }
            
            return View(await lots.AsNoTracking().ToListAsync());
        }

        /// <summary>
        /// Display error page
        /// </summary>
        /// <returns>View with error.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        /// <summary>
        /// Display page with user profile data.
        /// </summary>
        /// <returns>View with user profile data.</returns>
        public async Task<IActionResult> UserPage()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return RedirectToAction("Error", "Home");
            }
            EditUserViewModel model = new EditUserViewModel {Id = user.Id, UserName = user.Email, Name = user.Name, LastName = user.LastName, City = user.City, PhoneNumber = user.PhoneNumber};
            return View(model);
        }
 
        /// <summary>
        /// Get data from form and update user data.
        /// </summary>
        /// <param name="model">New user data</param>
        /// <returns>View with user profile data.</returns>
        [HttpPost]
        public async Task<IActionResult> UserPage(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);
                if(user!=null)
                {
                    user.Email = model.UserName;
                    user.UserName = model.UserName;
                    user.Name = model.Name;
                    user.LastName = model.LastName;
                    user.City = model.City;
                    user.PhoneNumber = model.PhoneNumber;

                    var result = await _userManager.UpdateAsync(user);
                    if (!result.Succeeded)
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            return View(model);
        }
    }
}
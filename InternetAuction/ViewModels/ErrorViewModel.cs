using System;

namespace InternetAuction.ViewModels
{
    /// <summary>
    /// Model of lot
    /// </summary>
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}